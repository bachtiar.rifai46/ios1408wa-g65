//
//  ViewController.swift
//  G65L4
//
//  Created by Ivan Vasilevich on 8/23/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
//		workWithString()
//		optionalType()
//		arrays()
		dictionaries()
	}
	
	func workWithString() {
		var name = "Ivan"
		let result = """
my name is "\(name)"
"""  as NSString
		let range = NSRange.init(location: 3, length: 7)
		let partOfResult = result.substring(with: range)
		print("partOfResult = \"\(partOfResult)\"")
		print(result, result.length)
		name += " Besarab"
		print("my name is \"\(name)\" it consist of \(name.count) characters")
		
		
		//NSMutableString from swift String
		let mutableStr = NSMutableString.init(string: "a")
//		mutableStr = NSMutableString.init(string: "a")
	}
	
	func optionalType() {
		let strWithNumberInside = "a4"
		let optionalNumberFromString = Int(strWithNumberInside)
		
		print(optionalNumberFromString! + optionalNumberFromString!)
		if let realNumberFromString = optionalNumberFromString {
			print(realNumberFromString + 5)
		}
		else {
			print("optionalNumberFromString = nil")
		}
		var studentsCount: Int?
		
		if arc4random()%666 > 333 {
			studentsCount = (studentsCount ?? 0) + 1
		}
		
		guard let realNumberFromString = optionalNumberFromString else {
			return
		}
		
		realNumberFromString
		
		
		
		
		
	}

	func arrays() {
		var buyList = ["6 Eggs",
					   "Bananas",
					   "Beer"]
		buyList.append("Gun")
		print("items to buy: \(buyList[1])")
		for i in 0..<buyList.count {
			print("item #\(i + 1) = \(buyList[i])")
		}
		
		var randomNumbers = [UInt32]()
		
		for _ in 0..<20 {
			randomNumbers.append(arc4random()%10)
		}
		print(randomNumbers)
		print(randomNumbers.index(of: 5))
	}
	
	func dictionaries() {
		var phoneBook = [
			101 : "Firefighter",
			102 : "Police",
			103 : "Ambulance",
			104 : "Gas"
		]
		
		phoneBook[101] = "GAS"
		let code = 101
		print("code \(code) = \(phoneBook[code] ?? "unknown") service")
	}

}

