//
//  ViewController.swift
//  G65L3
//
//  Created by Ivan Vasilevich on 8/21/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		let piNumber = 3.14159
		print("pi number = \(piNumber)")
		
		print("hellow hellow")
		
		var age = 22
		print("age = \(age)")
		age = 23
		print("age = \(age)")
		age = 25
		print("age = \(age), f = \(false)")
		
//		+ - * / %
		var a = 15
//		< > <= >= == !=
		if a < 20 {
			age = 8
			var f = age
			print("f =", f)
			f = 77
			print("f =", f)
		}
		
		var money = 1000000
//		&& ||
		var yaStariy = age <= 60
		while !yaStariy  || money > 100 {
			print("alive", age)
			age = age + 1
			age += 1
			money -= 10000
		}
		
		for i in 0..<age {
			print("i = \(i)")
		}
		print("retirement")
		
		a = Int(piNumber) + age
		age = a % 7
		print("age = \(age)")
//		let b: Double = 5
//		let c = 5.0
		randomNumbers(min: 0, max: 12)
		foo()
	}
	
	func randomNumbers(min: UInt32, max: UInt32) {
		let randomNumber = min + arc4random()%(max - min)
		print("Lucker#\(randomNumber)")
	}
	
	func foo() {
		print("foo() - CALLED")
		let summOfSquareOf5And6 = squareOfNumber(5) + squareOfNumber(6)
		print("summOfSquareOf5And6", summOfSquareOf5And6)
		randomDay() + 5
	}

	func squareOfNumber(_ number: Double) -> Double {
		let result = number * number
		print("\(number) * \(number) = \(result)")
		return result
	}
	
	func randomDay() -> Int {
		var result: Int = 0
		result = Int(arc4random()%7) + 1
		return result
	}


}

