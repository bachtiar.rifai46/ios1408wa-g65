//
//  ViewController.swift
//  G65L9
//
//  Created by Ivan Vasilevich on 9/11/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		let cm = CofeMashina4000.shared
		cm.coffe = "koffeeefffeffefe"
		cm.melk = "meeeeeeeelk"
		print(cm.getElements[0])
		
		let happyFace = FaceView(frame: CGRect(x: 100, y: 100, width: 128, height:128))
		happyFace.faceColor = .red
		happyFace.changeHappyFace(withHappLevel: 35, andColor: .red)
		view.addSubview(happyFace)
	}

}

