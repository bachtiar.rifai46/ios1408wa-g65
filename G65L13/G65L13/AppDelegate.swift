//
//  AppDelegate.swift
//  G65L13
//
//  Created by Ivan Vasilevich on 9/25/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit
import Parse

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?


	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		// Override point for customization after application launch.
		let config = ParseClientConfiguration { (conf) in
			conf.clientKey = "FXA6zLlemgn2FH1erTRnh6FCzN1G9qBuNfeNl9xr"
			conf.applicationId = "IFzg1Kqo2EHm596L6U0lm48XT6CYkORjhmCvLBqw"
			conf.server = "https://parseapi.back4app.com"
			//			conf.isLocalDatastoreEnabled = true
		}
		Parse.initialize(with: config)
		return true
	}


}

