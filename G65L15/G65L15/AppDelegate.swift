//
//  AppDelegate.swift
//  G63L14_3
//
//  Created by Ivan Vasilevich on 7/15/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?


	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		// Override point for customization after application launch.
		print(UserDefaults.standard.string(forKey: "name_preference") ?? "no name")
		return true
	}

}

