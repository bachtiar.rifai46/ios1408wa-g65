//
//  AlligatorVC.swift
//  G65L7
//
//  Created by Ivan Vasilevich on 9/4/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class AlligatorVC: UIViewController {
	
	@IBOutlet weak var messageFromKrokodilTextField: UITextField!
	var message: String?
	
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		messageFromKrokodilTextField.text = message
    }
	
	@IBAction func trashPressed(_ sender: UIBarButtonItem) {
		navigationController?.popViewController(animated: false)
	}
	
}
