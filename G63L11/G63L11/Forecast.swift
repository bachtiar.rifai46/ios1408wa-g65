//
//  File.swift
//  G63L11
//
//  Created by Ivan Vasilevich on 7/8/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import AlamofireObjectMapper
import ObjectMapper

class WeatherResponse: Mappable {
	var city: City?
	var list: [Forecast]?
	
	required init?(map: Map){
		
	}
	
	func mapping(map: Map) {
		city <- map["city"]
		list <- map["list"]
	}
}


class Forecast: Mappable {
	var temperature: Double = 0
	var conditions: String?
	
	required init?(map: Map){
		
	}
	
	func mapping(map: Map) {
		temperature <- map["main.temp"]
		temperature -= 273
		conditions <- map["weather.0.main"]
	}
}

class City: Mappable {
	var name: String?
	var id: Int?
	
	required init?(map: Map){
		
	}
	
	func mapping(map: Map) {
		name <- map["name"]
		id <- map["id"]
	}
}


